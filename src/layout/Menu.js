import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import SettingsIcon from '@material-ui/icons/Settings';
import { useMediaQuery } from '@material-ui/core';
import { useTranslate, MenuItemLink } from 'react-admin';

import categories from '../categories';
import subCategories from '../subCategories';
import Provider from '../provider';
import rewards from '../rewards'
import parameters from '../parameters'

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    Active: {
        '&:hover':{
            backgroundColor: '#E6E6E6', 
            borderLeft: '7px solid #00B7D7',
            transition: 'all .2s ease-in-out',
        },        
    },
}));

const Menu = ({ onMenuClick, dense, logout }) => {
    const classes = useStyles();
    const translate = useTranslate();
    const isXsmall = useMediaQuery(theme => theme.breakpoints.down('xs'));
    const open = useSelector(state => state.admin.ui.sidebarOpen);
    useSelector(state => state.theme); // force rerender on theme change

    return (
        <div className={classes.Border}>
            {' '}
            <MenuItemLink
                to={`/rewards`}
                primaryText={translate(`resources.rewards.name`, {
                    smart_count: 2,
                })}
                leftIcon={<rewards.icon />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                className={classes.Active}
                activeStyle={{
                    backgroundColor: '#E6E6E6', 
                    borderLeft: '7px solid #00B7D7',
                }}
            />
            <MenuItemLink
                to={`/categories`}
                primaryText={translate(`resources.categories.name`, {
                    smart_count: 2,
                })}
                leftIcon={<categories.icon />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                className={classes.Active}
                activeStyle={{
                    backgroundColor: '#E6E6E6', 
                    borderLeft: '7px solid #00B7D7',
                }}
            />
            <MenuItemLink
                to={`/subCategories`}
                primaryText={translate(`resources.subCategories.name`, {
                    smart_count: 2,
                })}
                leftIcon={<subCategories.icon />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                className={classes.Active}
                activeStyle={{
                    backgroundColor: '#E6E6E6', 
                    borderLeft: '7px solid #00B7D7',
                }}
            />
            <MenuItemLink
                to={`/provider`}
                primaryText={translate(`resources.provider.name`, {
                    smart_count: 2,
                })}
                leftIcon={<Provider.icon />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                className={classes.Active}
                activeStyle={{
                    backgroundColor: '#E6E6E6', 
                    borderLeft: '7px solid #00B7D7',
                }}
            />
            <MenuItemLink
                to={`/parameters`}
                primaryText={translate(`resources.parameters.name`, {
                    smart_count: 2,
                })}
                leftIcon={<parameters.icon />}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
                className={classes.Active}
                activeStyle={{
                    backgroundColor: '#E6E6E6', 
                    borderLeft: '7px solid #00B7D7',
                }}
            />
            {isXsmall && (
                <MenuItemLink
                    to="/configuration"
                    primaryText={translate('pos.configuration')}
                    leftIcon={<SettingsIcon />}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                />
            )}
            {isXsmall && logout}
        </div>
    );
};

Menu.propTypes = {
    onMenuClick: PropTypes.func,
    logout: PropTypes.object,
};

export default Menu;
