export const darkTheme = {
    palette: {
        type: 'dark', // Switching the dark mode on is a single property value change.
    },
};

export const lightTheme = {
    palette: {
        secondary: {
            light: '#5f5fc4',
            main: '#283593',
            dark: '#001064',
            contrastText: '#fff',
        },
        primary: {
            light: '#5f5fc4',
            main: '#00B7D7',
            dark: '#001064',
            contrastText: '#fff',
        },
    },
    overrides: {
        MuiFilledInput: {
            root: {
                backgroundColor: 'rgba(0, 0, 0, 0.04)',
                '&$disabled': {
                    backgroundColor: 'rgba(0, 0, 0, 0.04)',
                },
            },
        },
        MuiTableCell:{
             head:{
                fontWeight: 'bold',
                backgroundColor: 'Lavender',
                textAlign: 'center',
                height: '3rem'
             },
             body:{
                textAlign: 'center',
             }

         },
    },
    imageFieldStyle:{
        image: {
            maxHeight: '3rem'
        },
    },
};

export default lightTheme;
