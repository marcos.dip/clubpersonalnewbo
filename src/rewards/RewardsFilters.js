import React from 'react';
import {Filter, ReferenceInput, SearchInput, TextInput, DateInput } from 'react-admin';

const PostFilter = (props) => (
    <Filter {...props}>
        <SearchInput  source="q" alwaysOn autoComplete="on"/>
        <ReferenceInput label="Id" source="id" reference="rewards" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="Título" source="name" reference="rewards" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="Categoría" source="category.name" reference="rewards" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="SubCategoría" source="subcategory.name" reference="rewards" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="Proveedor" source="provider.name" reference="rewards" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="Fecha Desde" source="validityFrom" reference="rewards" allowEmpty>
        <DateInput/>
        </ReferenceInput>
        <ReferenceInput label="Fecha Hasta" source="validityTo" reference="rewards" allowEmpty>
            <DateInput/>
        </ReferenceInput>
    </Filter>
);

export default PostFilter;