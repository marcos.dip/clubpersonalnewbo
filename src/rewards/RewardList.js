import React from 'react';
import { Datagrid, List, TextField, DateField, ImageField} from 'react-admin';
import { withStyles} from '@material-ui/core/styles';
import PostFilter from './RewardsFilters'
import lightTheme from '../layout/themes';

const styles = ({
    image:lightTheme.imageFieldStyle.image
})

const RewardList =  withStyles(styles)(({classes, ...props}) => {
return(
    <List {...props} bulkActionButtons={false} filters={<PostFilter />}>
        <Datagrid classes={classes} rowClick="edit">
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="category.name" />
            <TextField source="subcategory.name" />
            <TextField source="provider.name" />
            <ImageField source="image" classes={{ image: classes.image }} sortable={false} />
            <DateField source="validityFrom" />
            <DateField source="validityTo" />
        </Datagrid>
    </List>
)});
export default RewardList;
