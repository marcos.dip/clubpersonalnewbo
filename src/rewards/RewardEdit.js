import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    Titulo: {
        color: '#283593',
        textalign: 'center',
    },
    caja: {
        height: '150px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

const Titulo = props => {
    const classes = useStyles();
    return <h2 className={classes.Titulo} >Hola Premios</h2>
}

const RewardEdit = props => {
    const classes = useStyles();
    return (
        <div className={classes.caja}>
            <Titulo />
        </div>
    );
};

export default RewardEdit;