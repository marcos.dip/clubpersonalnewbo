import RewardIcon from '@material-ui/icons/Redeem';

import RewardList from './RewardList';
import RewardEdit from './RewardEdit';

export default {
    list: RewardList,
    icon: RewardIcon,
    edit: RewardEdit,
};
