import React from 'react';
import { Datagrid, List, TextField, ImageField, Filter, SearchInput, ReferenceInput, TextInput } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import ColorInputComponent from '../customComponents/ColourComponent';
import lightTheme from '../layout/themes';

const styles = ({
    image:lightTheme.imageFieldStyle.image,
})

const PostFilter = (props) => (
    <Filter {...props}>
        <SearchInput  source="q" alwaysOn autoComplete="off"/>
        <ReferenceInput label="Nombre" source="name" reference="categories" allowEmpty>
            <TextInput/>
        </ReferenceInput>
        <ReferenceInput label="Color" source="colour" reference="categories" allowEmpty>
            <TextInput/>
        </ReferenceInput>
    </Filter>
);

const CategoryList = withStyles(styles)(({classes, ...props}) => {
    return (
    <List {...props} sort={{ field: 'name', order: 'ASC' }}  bulkActionButtons={false} filters={<PostFilter />}>
        <Datagrid classes={classes} rowClick="edit"> 
            <TextField source="name"/>
            <ColorInputComponent source="colour"/>
            <ImageField source="image" classes={{ image: classes.image }} sortable={false} />
        </Datagrid>
    </List>
    );
});

export default CategoryList;
