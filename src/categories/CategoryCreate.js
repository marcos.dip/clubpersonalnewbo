import React from 'react';
import { Create, TextInput, NumberInput, FormTab, TabbedForm } from 'react-admin';
import { createMuiTheme, withStyles } from '@material-ui/core/styles';

const listStyle = createMuiTheme({
    datagridStyle:{
        header: {
            fontWeight: 'bold',
            backgroundColor: 'Lavender',
            height: '3rem'
        },
    },
    imageFieldStyle:{
        image: {
            maxHeight: '3rem'
        },
    },
})
  
const styles = ({
    headerCell: listStyle.datagridStyle.header,
    image:listStyle.imageFieldStyle.image,
})

export const CategoryCreate = withStyles(styles)(({classes, ...props}) => {
    return (

    <Create {...props}>
        <TabbedForm>
            <FormTab label="Nuevo" >
                <TextInput source="name" />
                <NumberInput source="colour" />
            </FormTab>
        </TabbedForm>
    </Create>
    )
});