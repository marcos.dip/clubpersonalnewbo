import React, { useState, useEffect } from 'react';
import { Admin, Resource } from 'react-admin';
import polyglotI18nProvider from 'ra-i18n-polyglot';

import './App.css';

import authProvider from './authProvider';
import themeReducer from './themeReducer';
import { Login, Layout } from './layout';
import customRoutes from './routes';
import spanishMessages from './i18n/es'
import categories from './categories';
import provider from './provider';
import subCategory from './subCategories';
import dataProviderFactory from './dataProvider';
import rewards from './rewards';
import parameters from './parameters';
import { CategoryCreate } from './categories/CategoryCreate';
import {lightTheme } from './layout/themes';

const i18nProvider = polyglotI18nProvider(locale => {
    // Always fallback on english
    return spanishMessages;
}, 'es');

const App = () => {
    const [dataProvider, setDataProvider] = useState(null);
    const theme = lightTheme;

    useEffect(() => {
        let restoreFetch;

        const fetchDataProvider = async () => {
            setDataProvider(
                await dataProviderFactory(process.env.REACT_APP_DATA_PROVIDER)
            );
        };

        fetchDataProvider();

        return restoreFetch;
    }, []);

    if (!dataProvider) {
        return (
            <div className="loader-container">
                <div className="loader">Loading...</div>
            </div>
        );
    }

    return (
        <Admin
            title="Club Personal BackOffice"
            dataProvider={dataProvider}
            customReducers={{ theme: themeReducer }}
            customRoutes={customRoutes}
            authProvider={authProvider}
            loginPage={Login}
            layout={Layout}
            theme= {theme}
            i18nProvider={i18nProvider}
        >
            <Resource name="rewards" {...rewards} />
            <Resource name="categories" {...categories} create={CategoryCreate} />
            <Resource name="subCategories" {...subCategory}/>
            <Resource name="parameters" {...parameters} />
            <Resource name="provider" {...provider} />
        </Admin>
    );
};

export default App;
