import ProviderIcon from '@material-ui/icons/Book';

import ProviderList from './ProviderList';

export default {
    list: ProviderList,
    icon: ProviderIcon,
}