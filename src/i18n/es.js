export default {
    ra: {
        action: {
          delete: 'Borrar',
          show: 'Mostrar',
          list: 'Listar',
          save: 'Guardar',
          create: 'Crear',
          edit: 'Editar',
          sort: 'Ordenar',
          cancel: 'Cancelar',
          undo: 'Deshacer',
          refresh: 'Refrescar',
          add: 'Agregar',
          remove: 'Remover',
          add_filter: 'Agregar filtro',
          remove_filter: 'Remover este filtro',
          back: 'Regresar',
          bulk_actions: '%{smart_count} seleccionado',
          export: 'Exportar',
          search: 'Buscar'
        },
        boolean: {
          true: 'Si',
          false: 'No',
        },
        page: {
          list: 'Lista de %{name}',
          edit: '%{name} #%{id}',
          show: '%{name} #%{id}',
          create: 'Crear %{name}',
          dashboard: 'Dashboard',
          not_found: 'No encontrado',
          loading: 'Cargando',
          error: 'Error',
        },
        input: {
          file: {
            upload_several:
                           'Arrastra algunos archivos para empezar la subida, o presiona aqui para seleccionarlos.',
            upload_single: 'Arrastra un archivo para empezar la subida, or presiona aqui para seleccionarlo.',
          },
          image: {
            upload_several:
                           'Arrastra algunas images para empezar la subida, or persiona aqui para seleccionar una.',
            upload_single:
                          'Arrastra una imagen aqui para empezar la subida, or presiona aqui para seleccionarla.',
          },
          references: {
            all_missing: 'No se encontro ninguna referencia.',
            many_missing:
                         'Al menos una de las referencias asociadas parece ya no estar disponible.',
            single_missing:
                           'La referencia asociada parece ya no estar disponible.',
          },
        },
        message: {
          yes: 'Si',
          no: 'No',
          are_you_sure: 'Estas seguro?',
          about: 'Acerca de',
          not_found:
                    'Either you typed a wrong URL, or you followed a bad link.',
          loading: 'La pagina esta cargando, espera un momento por favor',
          invalid_form: 'El formulario no es valido. Por favor busca por errores',
          delete_title: 'Borrar %{name} #%{id}',
          delete_content: 'Estas seguro que quieres borrar este elemento?',
          bulk_delete_title:
                            'Borrar %{name} |||| Borrar %{smart_count} %{name} elementos',
          bulk_delete_content:
                              'Estas seguro de que quieres borrar este %{name}? |||| Estas seguro de que quieres borrar estos %{smart_count} elementos?',
          error: 'Error',
          details: 'Detalles',
        },
        navigation: {
          no_results: 'No se encontraron resultados',
          no_more_results:
                          'La pagina numero %{page} esta fuera de los limites. Prueba con la pagina anterior.',
          page_out_of_boundaries: 'La pagina numero %{page} se encuentra fuera de los limites',
          page_out_from_end: 'No se puede navegar despues de la ultima pagina',
          page_out_from_begin: 'No se puede navegar antes de la pagina 1',
          page_range_info: '%{offsetBegin}-%{offsetEnd} de %{total}',
          next: 'Siguiente',
          prev: 'Previo',
          page_rows_per_page: 'Registros por página',
        },
        auth: {
          username: 'Usuario',
          password: 'Contraseña',
          sign_in: 'Loguearse',
          sign_in_error: 'Error de autenticación, por favor reintenta',
          logout: 'Cerrar sesión',
          user_menu: 'Usuario logueado',
        },
        notification: {
          updated: 'Elemento actualizado |||| %{smart_count} elementos actualizados',
          created: 'Elemento creado',
          deleted: 'Elemento borrado |||| %{smart_count} elementos borrados',
          bad_item: 'Elemento incorrecto',
          item_doesnt_exist: 'El elemento no existe',
          http_error: 'Error de comunicacion con el servidor',
          canceled: 'Accion cancelada',
        },
        validation: {
          required: 'Requerido',
          minLength: 'Debe tener al menos %{min} caracteres',
          maxLength: 'Deber tener %{max} caracteres o menos',
          minValue: 'De ser al menos %{min}',
          maxValue: 'Debe ser %{max} o menos',
          number: 'Debe ser un number',
          email: 'Debe ser un correo electronico valido',
          oneOf: 'Debe ser uno de los siguientes valores: %{options}',
          regex: 'Debe seguir un formato especifico (regexp): %{pattern}',
        },
    },     
    pos: {
        search: 'Search',
        configuration: 'Configuración',
        language: 'Language',
        theme: {
            name: 'Theme',
            light: 'Light',
            dark: 'Dark',
        },
        dashboard: {
            monthly_revenue: 'Monthly Revenue',
            new_orders: 'New Orders',
            pending_reviews: 'Pending Reviews',
            new_customers: 'New Customers',
            pending_orders: 'Pending Orders',
            order: {
                items:
                    'by %{customer_name}, one item |||| by %{customer_name}, %{nb_items} items',
            },
            welcome: {
                title: 'Welcome to react-admin demo',
                subtitle:
                    "This is the admin of an imaginary poster shop. Feel free to explore and modify the data - it's local to your computer, and will reset each time you reload.",
                aor_button: 'react-admin site',
                demo_button: 'Source for this demo',
            },
        },
        menu: {
            sales: 'Sales',
            catalog: 'Catalog',
            customers: 'Customers',
        },
    },
    resources: {
        products: {
            name: 'Poster |||| Posters',
            fields: {
                category_id: 'Category',
                height_gte: 'Min height',
                height_lte: 'Max height',
                height: 'Height',
                image: 'Image',
                price: 'Price',
                reference: 'Reference',
                stock_lte: 'Low Stock',
                stock: 'Stock',
                thumbnail: 'Thumbnail',
                width_gte: 'Min width',
                width_lte: 'Max width',
                width: 'Width',
            },
            tabs: {
                image: 'Image',
                details: 'Details',
                description: 'Description',
                reviews: 'Reviews',
            },
        },
        categories: {
            name: 'Categoría |||| Categorías',
            fields: {
                products: 'Products',
                name: 'Nombre',
                colour: 'Color',
                image: 'Imagen',
            },
        },
        subCategories: {
            name: 'Sub Categoría |||| Sub Categorías',
            fields: {
                products: 'Products',
                name: 'Nombre',
                colour: 'Color',
                image: 'Imagen',
            },
        },
        provider: {
            name: 'Proveedor |||| Proveedores',
            fields: {
                
            }
        },
        rewards: {
            name: 'Premio |||| Premios',
            fields: {
                id: 'Id',
                name: 'Título',
                category: {
                    name :'Categoría'
                },
                subcategory: {
                    name:'Sub Categoría'
                },
                provider: {
                    name:'Proveedor'
                },
                image: 'Imagen',
                validityFrom: 'Vigencia desde',
                validityTo: 'Vigencia hasta'
            },
        },
        parameters: {
            name: 'Parámetro |||| Parámetros',
            fields: {
                id: 'Identificador',
                name: 'Nombre',
                value: 'Valor'
            },
        },
        segments: {
            name: 'Segments',
            fields: {
                customers: 'Customers',
                name: 'Name',
            },
            data: {
                compulsive: 'Compulsive',
                collector: 'Collector',
                ordered_once: 'Ordered once',
                regular: 'Regular',
                returns: 'Returns',
                reviewer: 'Reviewer',
            },
        },
    },
};
