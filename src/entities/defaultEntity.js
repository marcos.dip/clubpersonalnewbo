import APIUtils from  '../apiUtils';

export default class DefaultEntity {

    constructor(resourceName, embeddedKeys) {
        this.embeddedKeys = embeddedKeys;
    }

    getListRequest(params) {
        params.filter = {};
        return this.getListWithFilterRequest(params);
    };

    getListResponse(response, params) {
        response.content = APIUtils.createDummyEntities(params, response.content);
        if(!response.content) {
            return [];
        }
        return APIUtils.prepareGetListResponse(response.content, this.embeddedKeys);
    }
    createResponse(params, response) {
        return {
            data: {...params.data, id: response.content[0].id}
        };
    }
    getOneResponse(response) {
        if(response.content.length === 0) {
            return {};
        }

        return APIUtils.prepareGetOneResponse(response.content[0],
            this.embeddedKeys);
    }

    editResponse(response) {
        return APIUtils.prepareEditResponse(response, this.embeddedKeys);
    }

    getManyRequest(params) {
        return this.getListWithFilterRequest(params);
    }
}