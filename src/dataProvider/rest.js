import simpleRestProvider from 'ra-data-simple-rest';

const restProvider = simpleRestProvider('https://a11a108c-7242-4322-bd5d-44891d830055.mock.pstmn.io');

const delayedDataProvider = new Proxy(restProvider, {
    get: (target, name, self) =>
        name === 'then' // as we await for the dataProvider, JS calls then on it. We must trap that call or else the dataProvider will be called with the then method
            ? self
            : (resource, params) =>
                  new Promise(resolve =>
                      setTimeout(
                          () => resolve(restProvider[name](resource, params)),
                          500
                      )
                  ),
});

export default delayedDataProvider;

// import { fetchUtils } from 'react-admin';
// import { stringify } from 'query-string';
// import { reward } from '../entities';
// import {config} from '../config';

// const apiUrl = config.baseUrl;
// const httpClient = (url, options = {}) => {
//     if (!options.headers) {
//         options.headers = new Headers({ Accept: 'application/json' });
//     }
//     options.method = 'GET';
//     options.headers = new Headers({
//         'Content-Type': 'application/json',
//         'X-Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZW1vIiwiZXhwIjoxNTc1MjI0OTQ0fQ.f4jSEjz1sxOc594QP9sVBzNzAEUQ0vnXmGkYp6bo7aVAgEcitaMrNsg1e8fdp_luODwCfWrTU2Wc3N1RTbQzuA',
//         'Content-Range': 10
//     });
//     return fetchUtils.fetchJson(url, options);
// };

// const factory = function(resource) {
//     switch(resource) {
//         case "rewards": return reward;
//         // case "segments": return segment;
//         // case "commands": return command;
//         // case "products": return product;
//         // case "categories": return category;
//         // case "reviews": return review;
//         default: return null;
//     }
// }

// export default {
//     getList: (resource, params) => {
//         let entity = factory(resource);
//         const { page, perPage } = params.pagination;
//         const { field, order } = params.sort;
//         const query = {
//             sort: JSON.stringify([field, order]),
//             range: JSON.stringify([page, page * perPage]),
//             filter: JSON.stringify(params.filter),
//         };
//         const url = `${apiUrl}/${resource}?${stringify(query)}`;

//         return httpClient(url).then(({ headers, json }) => ({
//             data:  entity.getListResponse(json, params),
//             total: parseInt(10),
//         }));
//     },

//     getOne: (resource, params) =>
//         httpClient(`${apiUrl}/${resource}/${params.id}`).then(({ json }) => ({
//             data: json,
//         })),

//     getMany: (resource, params) => {
//         const query = {
//             filter: JSON.stringify({ id: params.ids }),
//         };
//         const url = `${apiUrl}/${resource}?${stringify(query)}`;
//         return httpClient(url).then(({ json }) => ({ data: json }));
//     },

//     getManyReference: (resource, params) => {
//         const { page, perPage } = params.pagination;
//         const { field, order } = params.sort;
//         const query = {
//             sort: JSON.stringify([field, order]),
//             range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
//             filter: JSON.stringify({
//                 ...params.filter,
//                 [params.target]: params.id,
//             }),
//         };
//         const url = `${apiUrl}/${resource}?${stringify(query)}`;

//         return httpClient(url).then(({ headers, json }) => ({
//             data: json,
//             total: parseInt(headers.get('content-range').split('/').pop(), 10),
//         }));
//     },

//     update: (resource, params) =>
//         httpClient(`${apiUrl}/${resource}/${params.id}`, {
//             method: 'PUT',
//             body: JSON.stringify(params.data),
//         }).then(({ json }) => ({ data: json })),

//     updateMany: (resource, params) => {
//         const query = {
//             filter: JSON.stringify({ id: params.ids}),
//         };
//         return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
//             method: 'PUT',
//             body: JSON.stringify(params.data),
//         }).then(({ json }) => ({ data: json }));
//     },

//     create: (resource, params) =>
//         httpClient(`${apiUrl}/${resource}`, {
//             method: 'POST',
//             body: JSON.stringify(params.data),
//         }).then(({ json }) => ({
//             data: { ...params.data, id: json.id },
//         })),

//     delete: (resource, params) =>
//         httpClient(`${apiUrl}/${resource}/${params.id}`, {
//             method: 'DELETE',
//         }).then(({ json }) => ({ data: json })),

//     deleteMany: (resource, params) => {
//         const query = {
//             filter: JSON.stringify({ id: params.ids}),
//         };
//         return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
//             method: 'DELETE',
//             body: JSON.stringify(params.data),
//         }).then(({ json }) => ({ data: json }));
//     },
// };