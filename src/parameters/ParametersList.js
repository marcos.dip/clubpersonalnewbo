import React from 'react';
import { Datagrid, List, TextField, Filter, ReferenceInput, SearchInput, TextInput } from 'react-admin';

const PostFilter = (props) => (
    <Filter {...props}>
        <SearchInput  source="q" alwaysOn autoComplete="off"/>
        <ReferenceInput label="Nombre" source="name" reference="parameters" >
            <TextInput optionText="name" />
        </ReferenceInput>
        <ReferenceInput label="Valor" source="value" reference="parameters" >
            <TextInput optionText="value" />
        </ReferenceInput>
    </Filter>
);
const ParametersList = (({ ...props }) => (
    <List  {...props} bulkActionButtons={false} 
    filters={<PostFilter />}>
        <Datagrid rowClick="edit">
            <TextField source="name" />
            <TextField source="value" />
        </Datagrid>
    </List>
));

export default ParametersList;