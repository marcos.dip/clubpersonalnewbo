import ParametersIcon from '@material-ui/icons/FormatListBulleted';

import ParametersList from './ParametersList';


export default {
    list: ParametersList,
    icon: ParametersIcon,
};