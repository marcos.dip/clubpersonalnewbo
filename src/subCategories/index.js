import subCategoryIcon from '@material-ui/icons/Bookmarks';

import subCategoryList from './subCategoryList';

export default {
    list: subCategoryList,
    icon: subCategoryIcon,
}